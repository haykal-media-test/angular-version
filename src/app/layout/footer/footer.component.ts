import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {


  innerWidth:number = 0;
  isMobile:boolean = false;
  
  @HostListener('window:resize', ['$event'])
  onResize() {
    if(window.innerWidth!=this.innerWidth) {
      this.innerWidth = window.innerWidth;
      this.checkIfItsMobileSize();
    }
  }
  
  constructor() { }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.checkIfItsMobileSize();
    console.log(this.isMobile);
  }

  checkIfItsMobileSize() {
    if(this.innerWidth <=600) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }
}
