import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ButtomFooterComponent } from './buttom-footer/buttom-footer.component';
import { ContainerComponent } from './container/container.component';



@NgModule({
  declarations: [NavbarComponent, FooterComponent, ButtomFooterComponent, ContainerComponent],
  imports: [
    CommonModule
  ],
  exports:[
    NavbarComponent, FooterComponent, ButtomFooterComponent,ContainerComponent
  ]
})
export class LayoutModule { }
