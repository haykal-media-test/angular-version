import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  @ViewChild('checbox') checbox:any;
  constructor() { }

  ngOnInit(): void {
  }

  clickOnMenu() {
    this.checbox.nativeElement.click();
    this.checbox.nativeElement.setAttribute("checked","false");
  }

}
