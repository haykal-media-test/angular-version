import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { SharedModule } from '../shared/shared.module';
import { LayoutModule } from '../layout/layout.module';
import { TechnicalSupportComponent } from './subscriptions/technical-support/technical-support.component';
import { RulesComponent } from './subscriptions/rules/rules.component';
import { SubscriptionAdditionalInfoComponent } from './subscriptions/subscription-additional-info/subscription-additional-info.component';
import { FeaturesComponent } from './subscriptions/features/features.component';
import { FeatureComponent } from './subscriptions/feature/feature.component';
import { PackagesComponent } from './subscriptions/packages/packages.component';
import { PackageComponent } from './subscriptions/package/package.component';


@NgModule({
  declarations: [
    HomeComponent, 
    SubscriptionsComponent, 
    TechnicalSupportComponent, 
    RulesComponent, 
    SubscriptionAdditionalInfoComponent, 
    FeaturesComponent, 
    FeatureComponent, 
    PackagesComponent, 
    PackageComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    LayoutModule
  ]
})
export class PagesModule { }
