import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';

const routes: Routes = [
  // {
  //   path:"",
  //   component:HomeComponent
  // },
  {
    path:"",
    component:SubscriptionsComponent
  },
  {
    path:"subscription",
    component:SubscriptionsComponent
  },
  {
    path:"**",
    redirectTo:"/"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
