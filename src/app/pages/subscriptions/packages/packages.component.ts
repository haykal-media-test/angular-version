import { Component, Input, OnInit } from '@angular/core';
import { Ipackage } from '../../models/package.interface';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {


  @Input('data') packages:Ipackage[] = [];
  
  constructor() { }

  ngOnInit(): void {
    console.log(this.packages);
  }


  trackBy(Package:Ipackage) {
    return Package.id;
  }

}
