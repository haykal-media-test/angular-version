import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PackagesService } from '../services/packages.service';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss']
})
export class SubscriptionsComponent implements OnInit {

  packages$:any;
  constructor(
    private _subscriptionsService:PackagesService
  ) { }

  ngOnInit(): void {
   this.packages$ = this._subscriptionsService.getPackages();
  }

}
