import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Ipackage } from '../../models/package.interface';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements OnInit {


  innerWidth:number = 0;

  @HostListener('window:resize', ['$event'])
  onResize() {
    if(window.innerWidth!=this.innerWidth) {
      this.innerWidth = window.innerWidth;
      this.displayAllFeatures();
    }
  }

  @Input('data') package:Ipackage = {} as Ipackage;
  @Output('seeMore') seeMoreClicked$ = new EventEmitter<{packageId:number,status:"seeMore" | "seeLess"}>();

  buttomContent:string;
  status:"seeMore" | "seeLess" = "seeMore";

  displayedFeatures:string[] = [];
  isMobile:boolean = false;
  
  constructor() {
    this.buttomContent = "";
  }

  ngOnInit(): void {
    
    this.innerWidth = window.innerWidth;

    if(this.innerWidth <=600)
      this.displayedFeatures = this.package.features.slice(0,2);
    else
      this.displayAllFeatures();
    this.buttomContent = "عرض كل ميزات الاشتراك";
  }

  displayAllFeatures() {
    this.status = "seeLess";
    this.buttomContent = "عرض ميزات أقل";
    this.displayedFeatures = this.package.features.slice(0);
  }

  toggleChanged() {
    
    this.seeMoreClicked$.emit({packageId:this.package.id,status:this.status});

    if(this.status === 'seeLess') {
      this.status = "seeMore";
      this.buttomContent = "عرض كل ميزات الاشتراك";
      this.displayedFeatures = this.package.features.slice(0,2);
    }
    else if(this.status === 'seeMore') {
      this.status = "seeLess";
      this.buttomContent = "عرض ميزات أقل"
      this.displayedFeatures = this.package.features.slice(0);
    }

  }
}
