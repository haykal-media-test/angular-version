import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionAdditionalInfoComponent } from './subscription-additional-info.component';

describe('SubscriptionAdditionalInfoComponent', () => {
  let component: SubscriptionAdditionalInfoComponent;
  let fixture: ComponentFixture<SubscriptionAdditionalInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscriptionAdditionalInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
