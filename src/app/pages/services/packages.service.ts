import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataSourceService } from 'src/app/shared/services/data-source/data-source.service';
import { Ipackage } from '../models/package.interface';
import { Package } from '../models/package.model';

const endPoint:string = "packages"
@Injectable({
  providedIn: 'root'
})
export class PackagesService extends DataSourceService{

  constructor() {
    super(endPoint);
  }

  getPackages() {
    
    return of(this.getPackagesDemo())
    .pipe();
        
    return this.getAll();

  }



  getPackagesDemo() {
    //fixed data
    const data:Ipackage[] = [
      {
        "id":1,
        "title":"اشتراك سنوي",
        "subTitle":"اشتراك رقمي فقط",
        "price":"$73",
        "discountAmount":"أفضل قيمة | وفر 47 دولار",
        "features":[
          "وصول رقمي المحتوى عبر التطبيق والموقع",
          "30 اصدار من المجلة",
          "300 فيديو قيم يثري معارلك",
          "5 آلاف مقالة تزدات يوميا",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء"
        ]
      },
      {
        "id":2,
        "title":"اشتراك 6 أشهر",
        "subTitle":"اشتراك رقمي فقط",
        "price":" كل سنة أشهر $73",
        "discountAmount":"وفر 28 دولار",
        "features":[
          "وصول رقمي المحتوى عبر التطبيق والموقع",
          "30 اصدار من المجلة",
          "300 فيديو قيم يثري معارلك",
          "5 آلاف مقالة تزدات يوميا",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء"
        ]
      },
      {
        "id":3,
        "title":"اشتراك شهري",
        "subTitle":"اشتراك رقمي فقط",
        "price":"10$ شهريا",
        discountAmount:"-",
        "features":[
          "وصول رقمي المحتوى عبر التطبيق والموقع",
          "30 اصدار من المجلة",
          "300 فيديو قيم يثري معارلك",
          "5 آلاف مقالة تزدات يوميا",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء"
        ]
      }
    ]

    return data;
    
  }
}
