export interface Ipackage {
    "id":number,
    "title":string,
    "subTitle":string,
    "price":string,
    "discountAmount":string,
    "features":string[];
}