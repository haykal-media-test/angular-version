import { Ipackage } from "./package.interface";

export class Package {
  
    id:number;
    title:string;
    subTitle:string;
    price:string;
    discountAmount:string;
    features:string[];
  
    constructor(subscription:Ipackage) {
      
      this.id = subscription.id || 0;
      this.title = subscription.title || "";
      this.subTitle = subscription.subTitle || "";
      this.price = subscription.price || "";
      this.features = subscription.features || [];
      this.discountAmount = subscription.discountAmount || "-";
  
    }
  
  }