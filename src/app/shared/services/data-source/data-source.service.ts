
import { Inject, Injectable } from '@angular/core';
import { HelperDataSourceMethodsService } from './helper-data-source-methods.service';
import { map , catchError } from "rxjs/operators";
import { Response } from 'src/app/core/models/response.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSourceService extends HelperDataSourceMethodsService{

  constructor(@Inject(String) endPoint:string) {
    super(endPoint);
  }

  get<T>(queryParams:any):Observable<T> {
    return this.http.get<Response<T>>(this.getFullUrl(),{params:queryParams})
    .pipe(
      map(this.prepareData),
      catchError(this.handleError)
    )
  }

  getAll<T>(resoruceName?:string):Observable<T> {
    return this.http.get<Response<T>>(this.getFullUrl(resoruceName))
    .pipe(
      map(this.prepareData),
      catchError(this.handleError)
    )
  }

  post<T>(data:any,resoruceName?:string):Observable<T> {
    return this.http.post<Response<T>>(this.getFullUrl(resoruceName),data)
    .pipe(
      map(this.prepareData),
      catchError(this.handleError)
    )
  }

  update<T>(data:any,resoruceName?:string):Observable<T> {
    return this.http.put<Response<T>>(this.getFullUrl(resoruceName),data)
    .pipe(
      map(this.prepareData),
      catchError(this.handleError)
    )
  }

  delete<T>(queryParams:any,resoruceName?:string):Observable<T> {
    return this.http.delete<Response<T>>(this.getFullUrl(resoruceName),{params:queryParams})
    .pipe(
      map(this.prepareData),
      catchError(this.handleError)
    )
  }

}
