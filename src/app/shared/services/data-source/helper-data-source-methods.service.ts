import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { appInjector } from 'src/app/app.module';
import { Response } from 'src/app/core/models/response.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HelperDataSourceMethodsService {

  http:HttpClient;
  private url:string;

  constructor(@Inject(String) endPoint:string) { 
    this.http = appInjector.get(HttpClient);
    this.url =`${environment.baseUrl}/${endPoint}/`;
  }

  getFullUrl(resrouceName?:string) {
    
    return resrouceName ? this.url + resrouceName : this.url;

  }

  protected prepareData<T>(response: Response<T>) {
    return response.data;
  }

  protected handleError(error: Response<Error>) {
    if (error instanceof HttpErrorResponse) {
      return throwError(error);
    }
    return throwError('Error');
  }
}
