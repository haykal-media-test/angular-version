import { Injectable, OnInit } from '@angular/core';


class LocalStorage implements Storage{

  constructor() {
    this.length = 0;
  }
  [name: string]: any;
  length: number;
  clear(): void {
    return;
  }
  getItem(key: string): string | null {
    return null;
  }
  key(index: number): string | null {
    return null;
  }
  removeItem(key: string): void {
  }
  setItem(key: string, value: string): void {
}

}

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService implements Storage,OnInit{


  [name: string]: any;
  length: number;

  storage:Storage;

  constructor() { 
    
    this.length = 0;
    this.storage = new LocalStorage();

  }
  ngOnInit(): void {
    
    this.storage = localStorage;

  }

  clear(): void {
    this.storage.clear();
  }
  getItem(key: string): string | null {
    return this.storage.getItem(key);
  }
  key(index: number): string | null {
    return this.storage.key(index);
  }
  removeItem(key: string): void {
    this.storage.removeItem(key);
  }
  setItem(key: string, value: string): void {
    this.storage.setItem(key,value)
  }
}
