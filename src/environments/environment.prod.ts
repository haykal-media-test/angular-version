export const environment = {
  production: true,
  baseUrl:"api",
  mobileSize: '(max-width: 600px)'
};
